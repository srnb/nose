onLoad in Global ~= (_ andThen ("project nose" :: _))

publish      := {}
publishLocal := {}

val publishSettings = Seq(
  organization         := "tf.bug",
  organizationName     := "bugtf",
  organizationHomepage := Some(url("https://bug.tf/")),
  scmInfo := Some(
    ScmInfo(
      url("https://gitlab.com/srnb/nose"),
      "scm:git@gitlab.com:srnb/nose.git"
    )
  ),
  developers := List(
    Developer(
      id = "srnb",
      name = "Anthony Cerruti",
      email = "me@s5.pm",
      url = url("https://s5.pm")
    )
  ),
  description := "Scala color library",
  licenses    := List("Apache-2.0" -> new URL("https://www.apache.org/licenses/LICENSE-2.0")),
  homepage    := Some(url("https://gitlab.com/srnb/nose")),
  // Remove all additional repository other than Maven Central from POM
  //  pomIncludeRepository := { _ =>
  //    false
  //  },
  publishTo := {
    val nexus = "https://oss.sonatype.org/"
    if (isSnapshot.value) Some("snapshots" at nexus + "content/repositories/snapshots")
    else Some("releases" at nexus + "service/local/staging/deploy/maven2")
  },
  publishMavenStyle    := true,
  publishConfiguration := publishConfiguration.value.withOverwrite(true),
  credentials += Credentials(Path.userHome / ".sbt" / ".credentials"),
)

val noseSettings = Seq(
  organization := "tf.bug",
  name := "nose",
  version := "0.2.0",
  scalaVersion := "2.13.0",
  crossScalaVersions := Seq("2.11.12", "2.12.8", "2.13.0"),
  mainClass := Some("tf.bug.nose.Main"),
  scalaJSUseMainModuleInitializer := true,
) ++ publishSettings

lazy val nose =
  (project in file("nose"))
    .enablePlugins(DynScalaJSPlugin)
    .settings(noseSettings)
    
