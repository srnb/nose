package tf.bug.nose
import tf.bug.nose.space.rgb.StandardRGB
import tf.bug.nose.space.rgb.RGB
import tf.bug.nose.space.cie.Illuminant.D65
import tf.bug.nose.space.rgb.RGBSpace.Standard
import tf.bug.nose.space.cie.XYZ
import tf.bug.nose.space.cie.Lab
import tf.bug.nose.space.HSV
import implicits._

object Main {

  def main(args: Array[String]): Unit = {
    val srgb = StandardRGB(0.5d, 0.0d, 0.5d)
    val lrgb = srgb.to[RGB[D65, Standard]].normalize
    val xyz = lrgb.to[XYZ[D65]].normalize
    val lab = xyz.to[Lab].normalize
    val backXyz = lab.to[XYZ[D65]].normalize
    val backLrgb = backXyz.to[RGB[D65, Standard]].normalize
    val backSrgb = backLrgb.to[StandardRGB].normalize
    val hsv = backSrgb.to[HSV].normalize
    println(s"""srgb: $srgb
               |lrgb: $lrgb
               |xyz:  $xyz
               |lab:  $lab
               |xyz:  $backXyz
               |lrgb: $backLrgb
               |srgb: $backSrgb
               |hsv:  $hsv""".stripMargin)
  }

}
