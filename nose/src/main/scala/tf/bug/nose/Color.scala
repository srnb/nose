package tf.bug.nose

trait Color[T] {

  type D

  def difference(a: T, b: T): D
  def addDifference(t: T, d: D): T
  def subDifference(t: T, d: D): T

  def addDifferences(a: D, b: D): D
  def subDifferences(a: D, b: D): D
  def mulDifference(d: D, s: Double): D

  def normalize(t: T): T = t
  def normalizeDifference(d: D): D = d

  def to[U](t: T)(implicit conv: ColorConversion[T, U]) = conv(t)

}

object Color {

  type Aux[α, β] = Color[α] { type D = β }

  trait Implicits {

    implicit class Ops[T, D](t: T)(implicit color: Color.Aux[T, D]) {

      def difference(b: T): D = color.difference(t, b)
      def addDifference(d: D): T = color.addDifference(t, d)
      def subDifference(d: D): T = color.subDifference(t, d)

      def ~(b: T): D = difference(b)
      def +~(d: D): T = addDifference(d)
      def -~(d: D): T = subDifference(d)

      def normalize: T = color.normalize(t)

      def to[U](implicit conv: ColorConversion[T, U]) = color.to[U](t)

    }

  }

}

trait ColorConversion[F, T] {

  def apply(from: F): T

}

object ColorConversion {

  trait Implicits {

    def identity[T]: ColorConversion[T, T] = new ColorConversion[T, T] {

      override def apply(from: T): T = from

    }

  }

}
