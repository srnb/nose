package tf.bug.nose

package space

package rgb

trait RGBSpace[S] {

  val xr: Double
  val yr: Double

  val xg: Double
  val yg: Double

  val xb: Double
  val yb: Double

}

object RGBSpace {

  sealed trait Standard
  sealed trait Adobe
  sealed trait P3

  trait Implicits {

    implicit def standardSpace: RGBSpace[Standard] = new RGBSpace[Standard] {

      override val xr: Double = 0.640d
      override val yr: Double = 0.330d

      override val xg: Double = 0.300d
      override val yg: Double = 0.600d

      override val xb: Double = 0.150d
      override val yb: Double = 0.060d

    }

    implicit def adobeSpace: RGBSpace[Adobe] = new RGBSpace[Adobe] {

      override val xr: Double = 0.640d
      override val yr: Double = 0.330d

      override val xg: Double = 0.210d
      override val yg: Double = 0.710d

      override val xb: Double = 0.150d
      override val yb: Double = 0.060d

    }

    implicit def p3Space: RGBSpace[P3] = new RGBSpace[P3] {

      override val xr: Double = 0.680d
      override val yr: Double = 0.320d

      override val xg: Double = 0.265d
      override val yg: Double = 0.690d

      override val xb: Double = 0.150d
      override val yb: Double = 0.060d

    }

  }

}
