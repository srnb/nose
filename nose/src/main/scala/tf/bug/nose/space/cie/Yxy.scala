package tf.bug.nose

package space

package cie

case class Yxy[L: Illuminant](yy: Double, x: Double, y: Double)
case class YxyDifference[L: Illuminant](
  yyDifference: Double,
  xDifference: Double,
  yDifference: Double
)

object Yxy {

  trait Implicits {

    implicit def yxyColor[L: Illuminant]: Color[Yxy[L]] = new Color[Yxy[L]] {

      override type D = YxyDifference[L]

      override def difference(a: Yxy[L], b: Yxy[L]): YxyDifference[L] =
        YxyDifference[L](a.yy - b.yy, a.x - b.x, a.y - b.y)

      override def addDifference(t: Yxy[L], d: YxyDifference[L]): Yxy[L] =
        Yxy[L](t.yy + d.yyDifference, t.x + d.xDifference, t.y + d.yDifference)

      override def subDifference(t: Yxy[L], d: YxyDifference[L]): Yxy[L] =
        Yxy[L](t.yy - d.yyDifference, t.x - d.xDifference, t.y - d.yDifference)

      override def addDifferences(
        a: YxyDifference[L],
        b: YxyDifference[L]
      ): YxyDifference[L] =
        YxyDifference[L](
          a.yyDifference + b.yyDifference,
          a.xDifference + b.xDifference,
          a.yDifference + b.yDifference
        )

      override def subDifferences(
        a: YxyDifference[L],
        b: YxyDifference[L]
      ): YxyDifference[L] =
        YxyDifference[L](
          a.yyDifference - b.yyDifference,
          a.xDifference - b.xDifference,
          a.yDifference - b.yDifference
        )

      override def mulDifference(
        d: YxyDifference[L],
        s: Double
      ): YxyDifference[L] =
        YxyDifference[L](
          d.yyDifference * s,
          d.xDifference * s,
          d.yDifference * s
        )

      override def normalize(t: Yxy[L]): Yxy[L] =
        Yxy[L](
          Math.max(0.0d, Math.min(1.0d, t.yy)),
          Math.max(0.0d, Math.min(1.0d, t.x)),
          Math.max(0.0d, Math.min(1.0d, t.y))
        )

      override def normalizeDifference(
        d: YxyDifference[L]
      ): YxyDifference[L] =
        YxyDifference[L](
          Math.max(-1.0d, Math.min(1.0d, d.yyDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.xDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.yDifference))
        )

    }

    implicit def yxyToXyz[L: Illuminant]: ColorConversion[Yxy[L], XYZ[L]] =
      new ColorConversion[Yxy[L], XYZ[L]] {

        override def apply(from: Yxy[L]): XYZ[L] = {
          val Yxy(yy, x, y) = from
          XYZ[L](x * (yy / y), yy, (1 - x - y) * (yy / y))
        }

      }

  }

}
