package tf.bug.nose

package space

package rgb

import tf.bug.nose.space.rgb.RGBSpace._
import tf.bug.nose.space.cie.{Illuminant, XYZ}, Illuminant.D65

case class RGB[L: Illuminant, S: RGBSpace](
  red: Double,
  green: Double,
  blue: Double
)
case class RGBDifference[L: Illuminant, S: RGBSpace](
  redDifference: Double,
  greenDifference: Double,
  blueDifference: Double
)

object RGB {

  trait Implicits {

    implicit def rgbColor[L: Illuminant, S: RGBSpace]
      : Color.Aux[RGB[L, S], RGBDifference[L, S]] =
      new Color[RGB[L, S]] {

        override type D = RGBDifference[L, S]

        override def difference(
          a: RGB[L, S],
          b: RGB[L, S]
        ): RGBDifference[L, S] =
          RGBDifference[L, S](
            a.red - b.red,
            a.green - b.green,
            a.blue - b.blue
          )

        override def addDifference(
          t: RGB[L, S],
          d: RGBDifference[L, S]
        ): RGB[L, S] =
          RGB[L, S](
            t.red + d.redDifference,
            t.green + d.greenDifference,
            t.blue + d.blueDifference
          )

        override def subDifference(
          t: RGB[L, S],
          d: RGBDifference[L, S]
        ): RGB[L, S] =
          RGB[L, S](
            t.red - d.redDifference,
            t.green - d.greenDifference,
            t.blue - d.blueDifference
          )

        override def addDifferences(
          a: RGBDifference[L, S],
          b: RGBDifference[L, S]
        ): RGBDifference[L, S] =
          RGBDifference[L, S](
            a.redDifference + b.redDifference,
            a.greenDifference + b.greenDifference,
            a.blueDifference + b.blueDifference
          )

        override def subDifferences(
          a: RGBDifference[L, S],
          b: RGBDifference[L, S]
        ): RGBDifference[L, S] =
          RGBDifference[L, S](
            a.redDifference - b.redDifference,
            a.greenDifference - b.greenDifference,
            a.blueDifference - b.blueDifference
          )

        override def mulDifference(
          d: RGBDifference[L, S],
          s: Double
        ): RGBDifference[L, S] =
          RGBDifference[L, S](
            d.redDifference * s,
            d.greenDifference * s,
            d.blueDifference * s
          )

        override def normalize(t: RGB[L, S]): RGB[L, S] =
          RGB[L, S](
            Math.max(0.0, Math.min(1.0, t.red)),
            Math.max(0.0, Math.min(1.0, t.green)),
            Math.max(0.0, Math.min(1.0, t.blue))
          )

        override def normalizeDifference(
          t: RGBDifference[L, S]
        ): RGBDifference[L, S] =
          RGBDifference[L, S](
            Math.max(-1.0, Math.min(1.0, t.redDifference)),
            Math.max(-1.0, Math.min(1.0, t.greenDifference)),
            Math.max(-1.0, Math.min(1.0, t.blueDifference))
          )

      }

    implicit def rgbToGammaRgb[T, D, L, S](
      implicit ev: GammaRGB.Aux[T, D, L, S],
      l: Illuminant[L],
      s: RGBSpace[S]
    ): ColorConversion[RGB[L, S], T] =
      new ColorConversion[RGB[L, S], T] {
        override def apply(
          from: RGB[L, S]
        ): T = {
          import ev.gamma
          val RGB(r, g, b) = from
          ev(gamma(r), gamma(g), gamma(b))
        }
      }

    implicit def gammaRgbToRgb[T, D, L, S](
      implicit ev: GammaRGB.Aux[T, D, L, S],
      l: Illuminant[L],
      s: RGBSpace[S]
    ): ColorConversion[T, RGB[L, S]] =
      new ColorConversion[T, RGB[L, S]] {
        override def apply(
          from: T
        ): RGB[L, S] = {
          import ev.inverseGamma
          val ev(r, g, b) = from
          RGB[L, S](inverseGamma(r), inverseGamma(g), inverseGamma(b))
        }
      }

    implicit def rgbToXyz[L: Illuminant, S: RGBSpace]
      : ColorConversion[RGB[L, S], XYZ[L]] =
      new ColorConversion[RGB[L, S], XYZ[L]] {
        override def apply(
          from: RGB[L, S]
        ): XYZ[L] = {
          val RGB(r, g, b) = from

          val (
            (xr, yr, zr, sr),
            (xg, yg, zg, sg),
            (xb, yb, zb, sb),
            (xw, yw, zw)
          ) = XYZ.getRGBWS[L, S]

          val (x, y, z) = (
            (b * sb * xb) + (g * sg * xg) + (r * sr * xr),
            (b * sb * yb) + (g * sg * yg) + (r * sr * yr),
            (b * sb * zb) + (g * sg * zg) + (r * sr * zr)
          )
          XYZ[L](x, y, z)
        }
      }

  }

}
