package tf.bug.nose

package space

package cie

trait Illuminant[L] {

  val xw: Double
  val yw: Double

  final lazy val xn: Double = (yn / yw) * xw
  final lazy val yn: Double = 1.0d
  final lazy val zn: Double = (yn / yw) * (1 - xw - yw)

}

object Illuminant {

  sealed trait D65
  sealed trait DCI

  trait Implicits {

    implicit def d65Illuminant: Illuminant[D65] = new Illuminant[D65] {

      override val xw: Double = 0.31271d
      override val yw: Double = 0.32902d

    }

    implicit def dciIlluminant: Illuminant[DCI] = new Illuminant[DCI] {

      override val xw: Double = 0.314
      override val yw: Double = 0.351

    }

  }

}
