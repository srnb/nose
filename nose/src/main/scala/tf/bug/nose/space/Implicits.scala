package tf.bug.nose

package space

trait Implicits
    extends rgb.RGB.Implicits with HSV.Implicits with rgb.StandardRGB.Implicits
    with rgb.RGBSpace.Implicits with cie.Illuminant.Implicits
    with cie.XYZ.Implicits with cie.Lab.Implicits with cie.Yxy.Implicits
