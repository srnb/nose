package tf.bug.nose

package space

package rgb

import tf.bug.nose.space.cie.Illuminant.D65
import tf.bug.nose.space.rgb.RGBSpace.Standard

case class StandardRGB(red: Double, green: Double, blue: Double)
case class StandardRGBDifference(
  redDifference: Double,
  greenDifference: Double,
  blueDifference: Double
)

object StandardRGB {

  trait Implicits {

    implicit def srgbColor
      : GammaRGB.Aux[StandardRGB, StandardRGBDifference, D65, Standard] =
      new GammaRGB[StandardRGB] {

        override type D = StandardRGBDifference

        override def difference(
          a: StandardRGB,
          b: StandardRGB
        ): StandardRGBDifference =
          StandardRGBDifference(
            a.red - b.red,
            a.green - b.green,
            a.blue - b.blue
          )

        override def addDifference(
          t: StandardRGB,
          d: StandardRGBDifference
        ): StandardRGB =
          StandardRGB(
            t.red + d.redDifference,
            t.green + d.greenDifference,
            t.blue + d.blueDifference
          )

        override def subDifference(
          t: StandardRGB,
          d: StandardRGBDifference
        ): StandardRGB =
          StandardRGB(
            t.red - d.redDifference,
            t.green - d.greenDifference,
            t.blue - d.blueDifference
          )

        override def addDifferences(
          a: StandardRGBDifference,
          b: StandardRGBDifference
        ): StandardRGBDifference =
          StandardRGBDifference(
            a.redDifference + b.redDifference,
            a.greenDifference + b.greenDifference,
            a.blueDifference + b.blueDifference
          )

        override def subDifferences(
          a: StandardRGBDifference,
          b: StandardRGBDifference
        ): StandardRGBDifference =
          StandardRGBDifference(
            a.redDifference - b.redDifference,
            a.greenDifference - b.greenDifference,
            a.blueDifference - b.blueDifference
          )

        override def mulDifference(
          d: StandardRGBDifference,
          s: Double
        ): StandardRGBDifference =
          StandardRGBDifference(
            d.redDifference * s,
            d.greenDifference * s,
            d.blueDifference * s
          )

        override def normalize(t: StandardRGB): StandardRGB =
          StandardRGB(
            Math.max(0.0d, Math.min(1.0d, t.red)),
            Math.max(0.0d, Math.min(1.0d, t.green)),
            Math.max(0.0d, Math.min(1.0d, t.blue))
          )

        override def normalizeDifference(
          d: StandardRGBDifference
        ): StandardRGBDifference = StandardRGBDifference(
          Math.max(-1.0d, Math.min(1.0d, d.redDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.greenDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.blueDifference))
        )

        override type L = D65
        override type S = Standard

        override def gamma(u: Double) =
          if (u <= 0.0031308d)
            12.92d * u
          else
            1.055d * Math.pow(u, 1.0d / 2.4d) - 0.055d

        override def inverseGamma(u: Double) =
          if (u <= 0.04045d)
            u / 12.92d
          else
            Math.pow((u + 0.055d) / 1.055d, 2.4d)

        override def apply(r: Double, g: Double, b: Double): StandardRGB =
          StandardRGB(r, g, b)

        override def unapply(t: StandardRGB): Option[(Double, Double, Double)] =
          Some((t.red, t.green, t.blue))

      }

    implicit def srgbToHsv: ColorConversion[StandardRGB, HSV] =
      new ColorConversion[StandardRGB, HSV] {

        override def apply(from: StandardRGB): HSV = {
          val max = Math.max(Math.max(from.red, from.green), from.blue)
          val min = Math.min(Math.min(from.red, from.green), from.blue)
          val preAdjHue = max match {
            case `min` => 0.0d
            case from.red =>
              60.0d * (0.0d + ((from.green - from.blue) / (max - min)))
            case from.green =>
              60.0d * (2.0d + ((from.blue - from.red) / (max - min)))
            case from.blue =>
              60.0d * (4.0d + ((from.red - from.green) / (max - min)))
          }
          val h = ((preAdjHue % 360.0d) + 360.0d) % 360.0d
          val s = max match {
            case 0.0d => 0.0d
            case _ => (max - min) / max
          }
          val v = max
          HSV(h, s, v)
        }

      }

  }

}
