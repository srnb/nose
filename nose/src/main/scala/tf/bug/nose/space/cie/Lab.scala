package tf.bug.nose

package space

package cie

case class Lab(l: Double, a: Double, b: Double)
case class LabDifference(
  lDifference: Double,
  aDifference: Double,
  bDifference: Double
)

object Lab {

  val delta: Double = 6.0d / 29.0d

  def f(t: Double) =
    if (t > Math.pow(delta, 3.0d))
      Math.cbrt(t)
    else
      (t / (3.0d * Math.pow(delta, 2.0d))) + (4.0d / 29.0d)

  def inversef(t: Double) =
    if (t > delta)
      Math.pow(t, 3.0d)
    else
      3 * Math.pow(delta, 2.0d) * (t - (4.0d / 29.0d))

  trait Implicits {

    implicit def labColor: Color.Aux[Lab, LabDifference] = new Color[Lab] {

      override type D = LabDifference

      override def difference(a: Lab, b: Lab): LabDifference =
        LabDifference(a.l - b.l, a.a - b.a, a.b - b.b)

      override def addDifference(t: Lab, d: LabDifference): Lab =
        Lab(t.l + d.lDifference, t.a + d.aDifference, t.b + d.bDifference)

      override def subDifference(t: Lab, d: LabDifference): Lab =
        Lab(t.l - d.lDifference, t.a - d.aDifference, t.b - d.bDifference)

      override def addDifferences(
        a: LabDifference,
        b: LabDifference
      ): LabDifference =
        LabDifference(
          a.lDifference + b.lDifference,
          a.aDifference + b.aDifference,
          a.bDifference + b.bDifference
        )

      override def subDifferences(
        a: LabDifference,
        b: LabDifference
      ): LabDifference =
        LabDifference(
          a.lDifference - b.lDifference,
          a.aDifference - b.aDifference,
          a.bDifference - b.bDifference
        )

      override def mulDifference(d: LabDifference, s: Double): LabDifference =
        LabDifference(d.lDifference * s, d.aDifference * s, d.bDifference * s)

      override def normalize(t: Lab): Lab =
        Lab(
          Math.max(0.0d, Math.min(100.0d, t.l)),
          Math.max(-128.0d, Math.min(128.0d, t.a)),
          Math.max(-128.0d, Math.min(128.0d, t.b))
        )

      override def normalizeDifference(d: LabDifference): LabDifference =
        LabDifference(
          Math.max(-100.0d, Math.min(100.0d, d.lDifference)),
          Math.max(-256.0d, Math.min(256.0d, d.aDifference)),
          Math.max(-256.0d, Math.min(256.0d, d.bDifference))
        )

    }

    implicit def labToXyz[L: Illuminant]: ColorConversion[Lab, XYZ[L]] =
      new ColorConversion[Lab, XYZ[L]] {

        override def apply(from: Lab): XYZ[L] = {
          val illuminant = implicitly[Illuminant[L]]
          val Lab(l, a, b) = from

          val x = illuminant.xn * inversef(
            ((l + 16.0d) / 116.0d) + (a / 500.0d)
          )
          val y = illuminant.yn * inversef((l + 16.0d) / 116.0d)
          val z = illuminant.zn * inversef(
            ((l + 16.0d) / 116.0d) - (b / 200.0d)
          )
          XYZ[L](x, y, z)
        }

      }

  }

}
