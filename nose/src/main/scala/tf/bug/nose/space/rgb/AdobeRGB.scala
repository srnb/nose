package tf.bug.nose

package space

package rgb

import tf.bug.nose.space.cie.Illuminant.D65
import tf.bug.nose.space.rgb.RGBSpace.Adobe

case class AdobeRGB(red: Double, green: Double, blue: Double)
case class AdobeRGBDifference(
  redDifference: Double,
  greenDifference: Double,
  blueDifference: Double
)

object AdobeRGB {

  trait Implicits {

    implicit def adobeRgbColor: GammaRGB.Aux[AdobeRGB, AdobeRGBDifference, D65, Adobe] =
      new GammaRGB[AdobeRGB] {

        override type D = AdobeRGBDifference

        override def difference(
          a: AdobeRGB,
          b: AdobeRGB
        ): AdobeRGBDifference =
          AdobeRGBDifference(
            a.red - b.red,
            a.green - b.green,
            a.blue - b.blue
          )

        override def addDifference(
          t: AdobeRGB,
          d: AdobeRGBDifference
        ): AdobeRGB =
          AdobeRGB(
            t.red + d.redDifference,
            t.green + d.greenDifference,
            t.blue + d.blueDifference
          )

        override def subDifference(
          t: AdobeRGB,
          d: AdobeRGBDifference
        ): AdobeRGB =
          AdobeRGB(
            t.red - d.redDifference,
            t.green - d.greenDifference,
            t.blue - d.blueDifference
          )

        override def addDifferences(
          a: AdobeRGBDifference,
          b: AdobeRGBDifference
        ): AdobeRGBDifference =
          AdobeRGBDifference(
            a.redDifference + b.redDifference,
            a.greenDifference + b.greenDifference,
            a.blueDifference + b.blueDifference
          )

        override def subDifferences(
          a: AdobeRGBDifference,
          b: AdobeRGBDifference
        ): AdobeRGBDifference =
          AdobeRGBDifference(
            a.redDifference - b.redDifference,
            a.greenDifference - b.greenDifference,
            a.blueDifference - b.blueDifference
          )

        override def mulDifference(
          d: AdobeRGBDifference,
          s: Double
        ): AdobeRGBDifference =
          AdobeRGBDifference(
            d.redDifference * s,
            d.greenDifference * s,
            d.blueDifference * s
          )

        override def normalize(t: AdobeRGB): AdobeRGB =
          AdobeRGB(
            Math.max(0.0d, Math.min(1.0d, t.red)),
            Math.max(0.0d, Math.min(1.0d, t.green)),
            Math.max(0.0d, Math.min(1.0d, t.blue))
          )

        override def normalizeDifference(
          d: AdobeRGBDifference
        ): AdobeRGBDifference = AdobeRGBDifference(
          Math.max(-1.0d, Math.min(1.0d, d.redDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.greenDifference)),
          Math.max(-1.0d, Math.min(1.0d, d.blueDifference))
        )

        override type L = D65
        override type S = Adobe

        override def gamma(u: Double) = Math.pow(u, 256.0d / 563.0d)

        override def inverseGamma(u: Double) = Math.pow(u, 563.0d / 256.0d)

        override def apply(r: Double, g: Double, b: Double): AdobeRGB = AdobeRGB(r, g, b)

        override def unapply(t: AdobeRGB): Option[(Double, Double, Double)] = Some((t.red, t.green, t.blue))

      }

  }

}
