package tf.bug.nose

package space

import tf.bug.nose.space.rgb.StandardRGB

case class HSV(hue: Double, saturation: Double, value: Double)
case class HSVDifference(
  hueDifference: Double,
  saturationDifference: Double,
  valueDifference: Double
)

object HSV {

  trait Implicits {

    implicit def hsvColor: Color.Aux[HSV, HSVDifference] =
      new Color[HSV] {

        override type D = HSVDifference

        override def difference(
          a: HSV,
          b: HSV
        ): HSVDifference =
          HSVDifference(
            a.hue - b.hue,
            a.saturation - b.saturation,
            a.value - b.value
          )

        override def addDifference(
          t: HSV,
          d: HSVDifference
        ): HSV =
          HSV(
            t.hue + d.hueDifference,
            t.saturation + d.saturationDifference,
            t.value + d.valueDifference
          )

        override def subDifference(
          t: HSV,
          d: HSVDifference
        ): HSV =
          HSV(
            t.hue - d.hueDifference,
            t.saturation - d.saturationDifference,
            t.value - d.valueDifference
          )

        override def addDifferences(
          a: HSVDifference,
          b: HSVDifference
        ): HSVDifference =
          HSVDifference(
            a.hueDifference + b.hueDifference,
            a.saturationDifference + b.saturationDifference,
            a.valueDifference + b.valueDifference
          )

        override def subDifferences(
          a: HSVDifference,
          b: HSVDifference
        ): HSVDifference =
          HSVDifference(
            a.hueDifference - b.hueDifference,
            a.saturationDifference - b.saturationDifference,
            a.valueDifference - b.valueDifference
          )

        override def mulDifference(
          d: HSVDifference,
          s: Double
        ): HSVDifference =
          HSVDifference(
            d.hueDifference * s,
            d.saturationDifference * s,
            d.valueDifference * s
          )

        override def normalize(
          t: HSV
        ): HSV =
          HSV(
            ((t.hue % 360.0d) + 360.0d) % 360.0d,
            Math.max(0.0d, Math.min(1.0d, t.saturation)),
            Math.max(0.0d, Math.min(1.0d, t.value))
          )

        override def normalizeDifference(
          d: HSVDifference
        ): HSVDifference =
          HSVDifference(
            d.hueDifference % 180.0d,
            Math.max(-1.0d, Math.min(1.0d, d.saturationDifference)),
            Math.max(-1.0d, Math.min(1.0d, d.valueDifference))
          )

      }

    implicit def hsvToRgb: ColorConversion[HSV, StandardRGB] =
      new ColorConversion[HSV, StandardRGB] {

        override def apply(from: HSV): StandardRGB = {
          val chroma = from.value * from.saturation
          val hueGroup = from.hue / 60
          val component = chroma * (1 - Math.abs((hueGroup % 2) - 1))
          val (cr, cg, cb) = hueGroup.floor match {
            case 0 => (chroma, component, 0.0d)
            case 1 => (component, chroma, 0.0d)
            case 2 => (0.0d, chroma, component)
            case 3 => (0.0d, component, chroma)
            case 4 => (component, 0.0d, chroma)
            case 5 => (chroma, 0.0d, component)
          }
          val offset = from.value - chroma
          val (r, g, b) = (cr + offset, cg + offset, cb + offset)
          StandardRGB(r, g, b)
        }

      }

  }

}
