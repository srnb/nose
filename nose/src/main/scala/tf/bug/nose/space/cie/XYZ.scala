package tf.bug.nose

package space

package cie

import tf.bug.nose.space.rgb.{RGB, RGBSpace}

case class XYZ[L: Illuminant](x: Double, y: Double, z: Double)
case class XYZDifference[L: Illuminant](
  xDifference: Double,
  yDifference: Double,
  zDifference: Double
)

object XYZ {

  def getRGBWS[L: Illuminant, S: RGBSpace]: (
    (Double, Double, Double, Double),
    (Double, Double, Double, Double),
    (Double, Double, Double, Double),
    (Double, Double, Double)
  ) = {
    val illuminant = implicitly[Illuminant[L]]
    val space = implicitly[RGBSpace[S]]

    val (xr, xg, xb) =
      (space.xr / space.yr, space.xg / space.yg, space.xb / space.yb)
    val (yr, yg, yb) = (1.0d, 1.0d, 1.0d)
    val (zr, zg, zb) = (
      (1.0d - space.xr - space.yr) / space.yr,
      (1.0d - space.xg - space.yg) / space.yg,
      (1.0d - space.xb - space.yb) / space.yb
    )
    val xw = illuminant.xn
    val yw = illuminant.yn
    val zw = illuminant.zn

    val ds = (xr * yg * zb) - (xg * yr * zb) - (xr * yb * zg) + (xb * yr * zg) + (xg * yb * zr) - (xb * yg * zr)

    val (sr, sg, sb) = (
      ((xw * (yg * zb - yb * zg))
        + (yw * (-xg * zb + xb * zg))
        + (zw * (xg * yb - xb * yg))) / ds,
      ((xw * (-yr * zb + yb * zr))
        + (yw * (xr * zb - xb * zr))
        + (zw * (-xr * yb + xb * yr))) / ds,
      ((xw * (yr * zg - yg * zr))
        + (yw * (-xr * zg + xg * zr))
        + (zw * (xr * yg - xg * yr))) / ds
    )

    (
      (xr, yr, zr, sr),
      (xg, yg, zg, sg),
      (xb, yb, zb, sb),
      (xw, yw, zw)
    )
  }

  trait Implicits {

    implicit def xyzColor[L: Illuminant]: Color.Aux[XYZ[L], XYZDifference[L]] =
      new Color[XYZ[L]] {

        override type D = XYZDifference[L]

        override def difference(a: XYZ[L], b: XYZ[L]): XYZDifference[L] =
          XYZDifference[L](a.x - b.x, a.y - b.y, a.z - b.z)

        override def addDifference(t: XYZ[L], d: XYZDifference[L]): XYZ[L] =
          XYZ[L](t.x + d.xDifference, t.y + d.yDifference, t.z + d.zDifference)

        override def subDifference(t: XYZ[L], d: XYZDifference[L]): XYZ[L] =
          XYZ[L](t.x - d.xDifference, t.y - d.yDifference, t.z - d.zDifference)

        override def addDifferences(
          a: XYZDifference[L],
          b: XYZDifference[L]
        ): XYZDifference[L] =
          XYZDifference[L](
            a.xDifference + b.xDifference,
            a.yDifference + b.yDifference,
            a.zDifference + b.zDifference
          )

        override def subDifferences(
          a: XYZDifference[L],
          b: XYZDifference[L]
        ): XYZDifference[L] =
          XYZDifference[L](
            a.xDifference - b.xDifference,
            a.yDifference - b.yDifference,
            a.zDifference - b.zDifference
          )

        override def mulDifference(
          d: XYZDifference[L],
          s: Double
        ): XYZDifference[L] =
          XYZDifference[L](
            d.xDifference * s,
            d.yDifference * s,
            d.zDifference * s
          )

        override def normalize(t: XYZ[L]): XYZ[L] =
          XYZ[L](
            Math.max(0.0d, Math.min(1.0d, t.x)),
            Math.max(0.0d, Math.min(1.0d, t.y)),
            Math.max(0.0d, Math.min(1.0d, t.z))
          )

        override def normalizeDifference(
          d: XYZDifference[L]
        ): XYZDifference[L] =
          XYZDifference[L](
            Math.max(-1.0d, Math.min(1.0d, d.xDifference)),
            Math.max(-1.0d, Math.min(1.0d, d.yDifference)),
            Math.max(-1.0d, Math.min(1.0d, d.zDifference))
          )

      }

    implicit def xyzToRgb[L: Illuminant, S: RGBSpace]
      : ColorConversion[XYZ[L], RGB[L, S]] =
      new ColorConversion[XYZ[L], RGB[L, S]] {

        /** @see http://brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
          *
          */
        override def apply(from: XYZ[L]): RGB[L, S] = {
          val XYZ(x, y, z) = from

          val (
            (xr, yr, zr, sr),
            (xg, yg, zg, sg),
            (xb, yb, zb, sb),
            (xw, yw, zw)
          ) = getRGBWS[L, S]

          val dm =
            (xr * yg * zb) -
              (xg * yr * zb) -
              (xr * yb * zg) +
              (xb * yr * zg) +
              (xg * yb * zr) -
              (xb * yg * zr)

          val (r, g, b) = (
            (
              (xg * yb * z) -
                (xb * yg * z) -
                (xg * y * zb) +
                (x * yg * zb) +
                (xb * y * zg) -
                (x * yb * zg)
            ) / (sr * dm),
            (
              (-xr * yb * z) +
                (xb * yr * z) +
                (xr * y * zb) -
                (x * yr * zb) -
                (xb * y * zr) +
                (x * yb * zr)
            ) / (sg * dm),
            (
              (xr * yg * z) -
                (xg * yr * z) -
                (xr * y * zg) +
                (x * yr * zg) +
                (xg * y * zr) -
                (x * yg * zr)
            ) / (sb * dm)
          )

          RGB[L, S](r, g, b)
        }

      }

    implicit def xyzToLab[L: Illuminant]: ColorConversion[XYZ[L], Lab] =
      new ColorConversion[XYZ[L], Lab] {

        override def apply(from: XYZ[L]): Lab = {
          val illuminant = implicitly[Illuminant[L]]
          val XYZ(x, y, z) = from

          val l = (116.0d * Lab.f(y / illuminant.yn)) - 16.0d
          val a = 500.0d * (Lab.f(x / illuminant.xn) - Lab.f(y / illuminant.yn))
          val b = 200.0d * (Lab.f(y / illuminant.yn) - Lab.f(z / illuminant.zn))
          Lab(l, a, b)
        }

      }

    implicit def xyzToYxy[L: Illuminant]: ColorConversion[XYZ[L], Yxy[L]] =
      new ColorConversion[XYZ[L], Yxy[L]] {

        override def apply(from: XYZ[L]): Yxy[L] = {
          val XYZ(x, y, z) = from
          Yxy[L](y, x / (x + y + z), y / (x + y + z))
        }

      }

  }

}
