package tf.bug.nose

package space

package rgb

trait GammaRGB[T] extends Color[T] {

  type L
  type S

  def gamma(u: Double): Double
  def inverseGamma(u: Double): Double

  def apply(r: Double, g: Double, b: Double): T
  def unapply(t: T): Option[(Double, Double, Double)]

}

object GammaRGB {

  type Aux[α, β, γ, δ] = GammaRGB[α] { type D = β; type L = γ; type S = δ }

}
